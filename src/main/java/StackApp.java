/**
 * @author Victor Zhdanov
 */
public class StackApp {

    public static void main(String[] args) {

        Stack stack = new Stack();
        Stack stack1 = new Stack(5);

        System.out.println("Initial");
        printStack(stack, stack1);

        stack.push(1);
        stack.push(2);
        stack.push("three");

        for (int i = 0; i < 10; i++) {
            stack1.push("Elem " + i);
        }

        System.out.println("After pushes");
        printStack(stack, stack1);

        stack.pop();
        for (int i = 0; i < 7; i++) {
            stack1.pop();
        }

        System.out.println("After pops"); //pops! gg (c)
        printStack(stack, stack1);

        stack.clearStack();
        stack1.clearStack();

        System.out.println("After clear");
        printStack(stack, stack1);

        Stack stack2 = new Stack(5);
        System.out.println(stack2);

        stack2.push("Hello");
        System.out.println("push Hello " + stack2);

        stack2.push("Mary");
        System.out.println("push Mary " + stack2);

        stack2.push("Lou");
        System.out.println("push Lou " + stack2);

        stack2.pop();
        System.out.println("pop Lou " + stack2);

        Object o = stack2.pop();
        System.out.println("pop Mary " + stack2 + ", returned " + o.toString());

        stack2.clearStack();
        System.out.println("clear " + stack2);

        stack2.reallocateStack(3);
        System.out.println("realloc 3 " + stack2);


    }

    private static void printStack(Stack stack, Stack stack1) {
        System.out.println("s: " + stack + "\ns1 " + stack1);
    }
}
