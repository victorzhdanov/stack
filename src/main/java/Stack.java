/**
 * @author Victor Zhdanov
 */

public class Stack {
    private Object[] stack;
    private int stackSize;
    private int stackCapacity;

    private static final int DEFAULT_CAPACITY = 15;

    public Stack() {
        this.stack = new Object[DEFAULT_CAPACITY];
        this.stackCapacity = this.stack.length;
        this.stackSize = 0;
    }

    public Stack(final int capacity) {
        this.stack = capacity > 0 ? new Object[capacity] : new Object[DEFAULT_CAPACITY];
        this.stackCapacity = this.stack.length;
        this.stackSize = 0;
    }

    public void push(final Object element) {
        if (this.stackSize >= this.stackCapacity) {
            reallocateStack();
        }

        this.stack[this.stackSize] = element;
        this.stackSize++;
    }

    public Object pop() {
        final Object element = this.stack[this.stackSize - 1];
        this.stack[this.stackSize - 1] = null;
        this.stackSize--;
        return element;
    }

    /**
     *
     * @return Max number of elements, that can be pushed to current stack.
     */
    public int getStackCapacity() {
        return this.stack.length;
    }

    /**
     *
     * @return Number of elements in current stack.
     */
    public int getStackSize() {
        int size = 0;

        for (int i = 0; i < this.stack.length; i++) {
            if (this.stack[i] != null) {
                size++;
            }
        }

        return size;
    }

    @Override
    public String toString() {
        String stackToString = "Stack=[";

        for (int i = 0; i < this.stack.length - 1; i++) {
            stackToString += this.stack[i] + ", ";
        }

        stackToString += this.stack[this.stackSize] + "]";

        stackToString += ", size=" + this.stackSize + ", capacity=" + this.stackCapacity;

        return stackToString;
    }

    /**
     * Allocates new stack capacity, that is sum of old stack
     * capacity and DEFAULT_CAPACITY.
     */
    void reallocateStack() {
        final Object[] temp = new Object[this.stack.length];
        final int oldLength = this.stack.length;

        for (int i = 0; i < temp.length; i++) {
            temp[i] = this.stack[i];
        }

        this.stack = new Object[oldLength + DEFAULT_CAPACITY];

        for (int i = 0; i < temp.length; i++) {
            this.stack[i] = temp[i];
            temp[i] = null;
        }

        this.stackCapacity = this.stack.length;
    }

    /**
     * Allocates new stack size.
     * If new size is less, than the number of elements in stack
     * before calling this method, new stack will include elements from 0
     * to new stack capacity. Other elements (with index greater, than
     * new stack capacity) will be ignored.
     * If new capacity is less than zero, {@link IllegalArgumentException}
     * will be thrown.
     *
     * @param newCapacity New capacity of stack
     * @throws IllegalArgumentException
     */
    void reallocateStack(final int newCapacity)
            throws IllegalArgumentException {

        if (newCapacity <= 0) throw new IllegalArgumentException("Argument must be greater than 0.");

        final Object[] temp = new Object[this.stack.length];

        for (int i = 0; i < temp.length; i++) {
            temp[i] = this.stack[i];
        }

        this.stack = new Object[newCapacity];

        if (newCapacity > temp.length) {
            for (int i = 0; i < temp.length; i++) {
                this.stack[i] = temp[i];
                temp[i] = null;
            }
        }
        else {
            for (int i = 0; i < newCapacity; i++) {
                this.stack[i] = temp[i];
                temp[i] = null;
            }
        }

        this.stackCapacity = this.stack.length;
    }

    /**
     * Removes all elements from stack and sets its default capacity.
     */
    void clearStack() {
        for (int i = 0; i < this.stack.length; i++) {
            this.stack[i] = null;
        }

        this.stackSize = 0;

        reallocateStack(DEFAULT_CAPACITY);
    }
}
