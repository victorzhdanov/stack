/**
 * @author Victor Zhdanov
 */
public class StackTest {
    @org.junit.Before
    public void setUp() throws Exception {
        Stack stack1 = new Stack();
        Stack stack2 = new Stack(25);
        Stack stack3 = new Stack();
        stack3.push(1);
        stack3.push(2);
        stack3.push("three");
    }

    @org.junit.After
    public void tearDown() throws Exception {

    }

    @org.junit.Test
    public void push() throws Exception {

    }

    @org.junit.Test
    public void pop() throws Exception {

    }

    @org.junit.Test
    public void getStackCapacity() throws Exception {

    }

    @org.junit.Test
    public void getStackSize() throws Exception {

    }

    @org.junit.Test
    public void reallocateStack() throws Exception {

    }

}